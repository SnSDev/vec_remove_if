# vec_remove_if

Extract elements from a vector based on supplied criteria

*Project by [SnS Development](https://gitlab.com/SnSDev)*

## Problem

Need to filter some elements out of an existing Vector through a mutable
borrow

## Solution

A trait implemented on `Vec<T>` with 2 functions `remove_if` and
`swap_remove_if` that iterate over elements, runs a supplied closure,
and removes elements where the closure returns [true].

## Example
```rust
use vec_remove_if::VecRemoveIf;
let mut v = vec![1, 12, 3, 14, 5, 16, 7, 18];

assert_eq!(
    vec![12, 14, 16, 18],
    v.remove_if(|e| e > &10)
);
assert_eq!(
    vec![1, 3, 5, 7],
    v
);
```

License: MIT
